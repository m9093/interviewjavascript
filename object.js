const person1 = {
    getFullName : function(){
        console.log(this.firstName + this.lastName)
    }
}

const person2 = {
    firstName : 'Phung',
    lastName : 'Sy',
    age : 18,
    gender : "male",
    getAge : ()=>{
        return this.age
    },
    getGender: function(){
        return this.gender
    },
    getAll(){
        return 'all'
    }
}

// person1.getFullName.apply(person2);   // PhungSy

// person1.getFullName.bind(person2);  // don't show any thing
// const fullName = person1.getFullName.bind(person2);  // don't show any thing
// fullName()    // PhungSY
// setTimeout(() => {
//     console.log(person2.getAge());
//     console.log(person2.getGender());
// }, 1000);

class Person{
    constructor(name , age){
        this.pname = name,
        this.age = age
    }
    set setname(name){
        this.pname = 'I am '+name;
    }
    get getName(){
        return this.pname
    }
}

const person = new Person("sy" , 18)

person.setname = 'phung'
Person.prototype.girlFriend = 'Ngoc Anh';
console.log(person.girlFriend);

