// const simulateAsync = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve("i love u");
//     }, 3000);
// });
// simulateAsync
//     .then((result) => {
//         console.log(result);
//     })
//     .catch((reject) => {
//         console.log(reject);
//     });

const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(1);
    }, 2000);
});
const promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject(2);
    }, 3000);
});
const promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(3);
    }, 3000);
});
const promise4 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(4);
    }, 3000);
});
async function doParalle(){
    const result1 = await promise1;
    const result2 = await promise2;
    const result3 = await promise3;
    const result4 = await promise4;
    return [result1 , result2 , result3 , result4 ]
}
function doParalle(){
    return Promise.all([promise1 , promise2 , promise3 , promise4])
}
// doSequence().then(result=>console.log(result))
doParalle().then(result=>console.log(result)).catch(e=>{
    console.log(e)
})

