# Javascript revise

## Scope
in js before ES6 only has global scope and function scope  
    - a variable declare outside any function become a global variable , when a global anywhere can access
    - a variable declare without var , let , const keyword is a global variable even if we defined it inside a function 
    - funtion scope : each function create a function scope  , variable create in function called **local variable** it will be deleted function complete
ES6 introduced Block scope - which code in { } for let and const
    - let , const in difference block cannot access other block 


##  Hoisting And Var , let , const

**var** : declare with 'var' be removed to top when compile 
```
    console.log(x)
    var x = 5;
    var x = 10;
    console.log(x)
=====================compile=======================
    var x ;
    console.log(x);   // undefined
    x = 5;
    x = 10;
    console.log(x)    // 10
```
**var** has function scope 
```
    var a = 5;    
    (function(){
        var b = 5;
        a = 10;
    })()
    console.l// outer function must be assign function
  result = (function add() {
    let counter = 0;     // private variable
    function plus() {    
        counter += 1;
        console.log(counter);
    }
    return plus;    // return a function
  })()      
   
  result();    //only call result() may increase counter
  result();
  result();og(a) // 10
    console.log(b)   // error
```
**var** don't have block scope
```
    {
        var a = 5;
    }
    console.log(a)  //5

```
**var** has globale scope and can redeclare anywhere
```
    index1.js :
        let a = 5 ; 
        // do something
    index2.js:
        console.log(a)  //5
        a = 10 ;
    index3.js:
        console.log(a)  //10    
    index.html : <script src='index1.js'/>
                 <script src='index2.js'/>    
                 <script src='index3.js'/>    

```
------------------
**let** : introduced in ES6(2015) has block scope  , 'let' cannot redeclared in same block 
```
    let name = 'bella';
    {
        let name = 'anna'
        console.log(name)  //anna
    }
    console.log(name)  //bella

    let myName = 'Anna';
    let myName = 'Jolie'    // identifier myName already has been declared
    (function setName(newName){
        let myName = newName;   // can redefined here
        let myNameUpperCase = newName.toUpperCase()
        console.log(myName);       // Bella
    })('Bella')
    console.log(myName);  //Anna
    console.log(myNameUpperCase);    // myNameUpperCase is not defined

   

```
**let** có hoisting và đựoc đưa lên đầu block hiện tại nhưng sử dụng truớc khi khai bao sẽ bị lỗi

----------------------

**Const** : introduced from ES6 , const same with let but cannot reassign , and alway assigned when declared
```
    const MAX = 10;
    MAX = 5;
    console.log(MAX)
```
 **const** not define constant value , it define constant reference to a value so that we can change element of an constant array , properties of constant object  
```
    const cars = ['audi' , 'BMW' , 'volvo'];
    cars[0] = 'Kia'
    cars.push("Cambri")
    cars = ["vinfast"]  //error
```
**const** có hoisting và đựoc đưa lên đầu block hiện tại nhưng sử dụng truớc khi khai bao sẽ bị lỗi

## Strict Mode 
From ECMAScript version 5 has 'use strict' , with 'use strict' with strict mode we can't do something like , we can apply strict mode to module by add "use strict"; at begining of file. 

    + use undeclared variable
    + delete variable , object , function , eval() , some keyword for future js version 
## This Keyword
    ```
        const person = {
            firstName: "John",
            lastName : "Doe",
            id       : 5566,
            fullName : function(separateString) {
                return this.firstName + separateString + this.lastName;
            }
        };
    ```
    In javascrip **this** refer to **An object**
    - In Object method this refer to this object , but use in call back will lost this 
    - Alone , **this** refer to global object - which is window on browser enviroment and module obj in node env
    - In function this refer to global object , but if strict mode this refer undefined
    - In event this refer to element which received this eventtrigger
    - method call() , apply() , bind() can refer **this** to any obj

    use Call() , apply() to call an object method with another object argument
    use bind() obj argument can borrow method from obj and return a function borrow , bind usually used when we have callback function which used **this** inside before ES6 , since ES6 we use arrow function instead
    ```
    const person1 = {
        fullName: function(separateString) {
            return this.firstName + separateString + this.lastName;
        }
    }

    const person2 = {
        firstName:"John",
        lastName: "Doe",
    }

        
    person1.fullName.call(person2,"this is separate arguments ") ; // Return "John Doe":
    person1.fullName.apply(person2,["this is separate arguments"]) ; // Return "John Doe":
    const getFullName = person1.getFullName.bind(person2)
    setTimeOut(()=>person1.fullName.call(person2,"this is separate arguments") , 2000)  //  this is separate arguments 
    setTimeOut(()=>getFullName , 2000)  // Phung  this is separate arguments Sy

    getFullName() // Return "John Doe": 
    ```

    precedence to determine which object this refer : bind() --> call(),apply() --> object method --> global scope

    
## Variable 
    In js we has : 
    **number**  : 0,1,2 ,NaN 
    **bigint** : in js we have range MAX_SAFE_INTEGER , only we use number out of this range shoule use bigint Type 
    **boolean** : true , false
    **string**: ,
    **object**: null is object , function should be should object although typeof function return 'function'. Because function also has properties(arguments ,prototype, name ,) and method (toString() , bind() , apply() )
    
    **undefined**: variable without value ==> undefined 
## Function
    - function declaration 
    ```
        sum(1,2) //3 because of function is hoisting
        function sum(a,b){
            return a+b
        }
        sum(1,2)  //3
    ```
    - function expression make dynamic and not hoisted
    ```
        subtract(2,6) // error even thought use 'var substract = function...' ;

        const subtract = function(a,b){
            return a - b
        }
        subtract(2,6) //-4;
        substract = sum;
        substract(2,6) //8
    ```
    - function also has constructor but we actually never use 
    ```
        const myFunction = new Function("a", "b", "return a * b");
        let x = myFunction(4, 3);
    ```
    - Function params are the names listed in function defination , function arguments are the real value pass to function
    in Js function don't check type and number of parameter 
        + if parameter is missing it assigned as **undefined** , from ES6 we can assign default value for parameter
    ```
        function random(x=0 , y =0){
            return x > y ? x : y;
        }
    ```
        + Rest Params , if don't know number of parameter we can use rest params to treat , restParams is an array contains all arguments pass to function 
    ```
        function sumAll(...params){
            let result = 0;
            for (argument of params){
                result +=argument;
            }
            return result;
        }
    ``` 
        + arguments object of function , by default all arguments pass to function store to arguments obj
    - self-invoke function 
    ```
        {
            function(){
                code
            }
        }()
    ```
        + Named params , we can pass argument and specified what is it , but use this way when update function params we must update where call function
    ```
        function substract(number1 , number2){
            return number1 - number2
        }
        subtract(number1 = 5 , number2 = 3)  //2
    ```
    - arrow function to write shorter syntax for function assignment which introduced from ES6
    arrow function not allow binding this , this alway refer to object which defined function
    ```
        const power = num => num*num;

        const person1 = {
            fullName: function() {
                return this.firstName + " " + this.lastName;
            }
        }

        const person2 = {
            firstName:"John",
            lastName: "Doe",
        }
        person1.fullName.call(person2) // NaN
        person1.fullName.apply(person2) // NaN
        const getFullName = person1.fullName.bind(person2) 
        getFullName() //NaN

    ```
    - pass referrence to obj and change will change original value 

    - closure Function : closure function used to create private variable belong to function only invoke function should change private variable 
    ```
        // outer function must be assign function
        result = (function add() {
            let counter = 0;     // private variable
            function plus() {    
                counter += 1;
                console.log(counter);
                return counter
            }
            return plus;    // return a function
        })()      
        
        result();    //only call result() may increase counter
        result();
        result();
    ```

## Class And Object
    - From ES6 class introduced, in class always has constructor to initialize obj properties , constructor named exactly constructor
```
    class Person{
        constructor(name , age){
            this.name = name ;
            this.age = age
        };
        getName(){

        }
    }
```
    - A class can inheritance another class by use keyword **extends** , when extends , children can use all method of parent and refer to parent by use keyword **super** , and class not hoisting
```
    class Male extends Person{
        constructor(name , age , salary){
            super(name , age);
            this.salary = salary
        }
        get getName(){
            return this.name
        }
        set setName(name){
            this.name = name
        }
    }
```  
    - We can add properties , mothods to object 
```
    Male.prototype.girlfriend = 'Anh'
```    
   

## JS Async 
 Async code has 2 type : 
    + BrowserApi / WebApi like setTimeOut() , setInterval() , click , scroll
    + Promise 

- A function can pass to another function like argument , in this case function passed called callback 
```
    function getAsync(url, callback){
        ...
        callback()
    }
```
 when use callback we may be face callback hell 

- Promise Object in Js received a function with two arguments  
 ```
    let myPromise = new Promise(function(myResolve, myReject) {
        // "Producing Code" (May take some time)
        myResolve(); // when successful
        myReject();  // when error
    });

    // "Consuming Code" (Must wait for a fulfilled Promise)
    myPromise.then(
        function(value) { /* code if successful */ },
        function(error) { /* code if some error */ }
    ).catch(function(error){
        { /* code if some error */ }
    }).finally(function(){
         { /* code always run */ }
    });
 ```
Promise Obj has 2 property : state , result . Both of them can't access
state has three status : pending , fullfilled , rejected and result corresponding is : undefined , value , error obj
Promise has method .all() phuơng thức này nhận vào một mảng Promise và trả về result là 1 Promise . Promise khi fullfiled trả về một mảng các kết quả tuơng ứng , truờng hợp 1 promise bị reject thì tất cả bị hủy, và result nhảy vào catch()
truờng hợp dùng allSetted thì result trả về khi tất cả promise thực hiện xong kể cả thành công hay thất bại

```
    const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(1);
    }, 2000);
    });
    const promise2 = new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(2);
        }, 3000);
    });
    const promise3 = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(3);
        }, 3000);
    });
    const result = Promise.all([promise1 , promise3])  
    const result2 = Promise.all([promise1 ,promise2 ,  promise3])  
    result.then(value => [1,3])
    result2.then().catch(e=> error : 2 )
```


- Async/await : introduced from EcmaScript 2017 ,  async and await make Promise easier to write . async makes a function return a Promise , await only use inside async function and makes a function wait for a Promise 
two function is same
```
    async function myFunction() {
        return "Hello";
    }
    function myFunction() {
        return Promise.resolve("Hello");
    }
```