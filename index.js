// var
console.log(a);
var a = 5;
var a = 10;
console.log(a);

// function scope of var
(function(){
    var b = 5;
})();
// console.log(b);

console.log('=====================================');

let myName = 'sy';
// console.log('mygirlFriend',mygirlFriend);
// let mygirlFriend = 'anh';
(function setName(newName){
    myName = 'sida';
    // console.log(myName);  // cannot access myName out of block
    let myName = newName;
    console.log(myName);
})('bella')
console.log(myName);

(function cacal(value){
    function parseValue(){
        console.log('children block' , value);  //5
        return value + 1;
    }
    return parseValue()+1
})(5);


console.log('=====================================');

// const MIN;   // error becase dont assign value
const MAX = 10;
// MAX = 5;  // error because assign to constant
console.log(MAX)