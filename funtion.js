// const person1 = {
//     getFullName : ()=>{
//         console.log(this.firstName + this.lastName)
//     }
// }

// const person2 = {
//     firstName : 'Phung',
//     lastName : 'Sy'    
// }

// person1.getFullName.call(person2);
// person1.getFullName.apply(person2);
// const getFullName = person1.getFullName.bind(person2);
// getFullName();


// function sum(number1 , number2){
//     return number1 + number2
// }
// function substract(number1 , number2){
//     return number1 - number2
// }
// console.log('named params',substract(number2=5 , number1 = 3));
// console.log(sum(3,5).arguments);

// function sumAll(...params){
//     let result = 0;
//     for (argument of params){
//         result +=argument;
//     }
//     return result;
// }
// console.log(sumAll(1,2,3,4,5,6));


// const add = (function () {
//     let counter = 0;
//     return function () {counter += 1; return counter}
//   })();
  
//   add();
//   add();
//   add();
//   console.log(add());


// outer function must be assign function
  result = (function add() {
    let counter = 0;     // private variable
    function plus() {    
        counter += 1;
        console.log(counter);
        return counter
    }
    return plus;    // return a function
  })()      
   
  result();    //only call result() may increase counter
  result();
  result();
  